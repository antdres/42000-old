 
 ┌───────┬───────────────────────┬──────────────────────────┬──────────────────────────────────────────────────────────────────────────────┐
 │ ID    │ Service Name          │ Short Description        │ Commads Available                                                            │
 ├───────┼───────────────────────┼──────────────────────────┼──────────────────────────────────────────────────────────────────────────────┤
 │ 42001 │ ANTSMGR               │ Server Manager Website   │ start, stop, restart, delete                                                 │
 │ 42002 │ ANTSSTS               │ Server Status Website    │ start, stop, restart, delete                                                 │
 │ 42003 │ anthill.sh            │ Website                  │ start, stop, restart, delete                                                 │
 │ 42004 │ andresbarrones.com    │ Website                  │ start, stop, restart, delete                                                 │
 │ 42005 │ Discord BOT           │ Discord JS               │ start, stop, restart, delete                                                 │
 │ 42006 │ YatoCraft             │ Linux GSM                │ start, stop, restart, details, update, update-lgsm, backup, console, debug   │
 │ 42007 │ CsArena               │ Linux GSM                │ start, stop, restart, details, update, update-lgsm, backup, console, debug   │
 │ 42008 │ CsFFA                 │ Linux GSM                │ start, stop, restart, details, update, update-lgsm, backup, console, debug   │
 └───────┴───────────────────────┴──────────────────────────┴──────────────────────────────────────────────────────────────────────────────┘
 